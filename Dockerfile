FROM registry.gitlab.com/mbio/mbiosphere/py-base:latest
COPY ./requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
RUN grep avx /proc/cpuinfo
COPY ./test.py /tmp/test.py
CMD python -c "import tensorflow as tf; print(tf.random.normal([1000, 1000]))"
