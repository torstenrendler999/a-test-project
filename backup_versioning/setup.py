import setuptools
import re

def get_version_info():
    try:
        with open('build_info') as fid:
            build_info = fid.readline()
        version_info = re.search('RELEASE_VERSION=(.+?)\n', build_info).group(1)
    except:
        version_infO = 'undefined'
    return version_info

setuptools.setup(name="bla",
                 version=get_version_info(),  # .dev{commitcount}+{gitsha}',
                 author="torstenrendler999",
                 author_email="torstenrendler999@gmail.com",
                 packages=setuptools.find_packages(),
                 )
