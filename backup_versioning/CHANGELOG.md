# CHANGELOG

<!--- next entry here -->

## 0.0.6
2020-03-01

### Fixes

- test (eb665e8d6d238ed6593302cfbde2f5c5984e270b)

## 0.0.5
2020-02-29

### Fixes

- correct setuptools (b3db8aaa1cddb4e02ac7320f79a30b6497ee9798)

## 0.0.4
2020-02-29

### Fixes

- autoversioning (6c40b9934e57503127788f8b875f88e5cb71d45e)
- corret get_version_info (e469e40fbed48f0b2958063746947c8cbb1703f1)

## 0.0.3
2020-02-29

### Fixes

- mod setup.py (a4503a00e8ae80d954e7c6fccd4230228c14ac0a)

## 0.0.2
2020-02-29

### Fixes

- test (4d1d3341fdcfbb1094036ea2f7eda9e43dc75de8)
- ci stage (e2aaa8c5424a100e5b988b06480c0af86f6e3241)

## 0.0.1
2020-02-29

### Fixes

- README.md (13bc0e5f0bbc5d93a87894cea7a79235700e022f)
- README.md (89a7102db8e08193f80a1d298c7eeba78c308164)
- .gitlab-ci.yml (a1a4002277fc89fd81e016db32cbc64080f5319b)